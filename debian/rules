#!/usr/bin/make -f
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This has to be exported to make some magic below work.
#export DH_OPTIONS

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

install_file    = /usr/bin/install -p -o root -g root -m 644
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
arch = $(shell dpkg-architecture -qDEB_BUILD_ARCH)

# Which MPI implementation?
# set ARCH_DEFAULT_MPI_IMPL
include /usr/share/mpi-default-dev/debian_defaults

ifneq ($(wildcard /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/$(ARCH_DEFAULT_MPI_IMPL)/libhdf5.so),)
  export DEB_CPPFLAGS_MAINT_APPEND := -I/usr/include/hdf5/$(ARCH_DEFAULT_MPI_IMPL)
  export DEB_LDFLAGS_MAINT_APPEND := -Wl,-L/usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/$(ARCH_DEFAULT_MPI_IMPL)
endif

CONFIGURE_FLAGS = --host=$(DEB_HOST_GNU_TYPE) --build=$(DEB_BUILD_GNU_TYPE) \
	--prefix=/usr --mandir=\$${prefix}/share/man \
	--infodir=\$${prefix}/share/info \
	--with-libctl=/usr/share/libctl \
	--with-gcc-arch=no \
	--enable-shared --with-pic --enable-cxx \
	--with-mpi=yes \
	--enable-maintainer-mode

%:
	dh $@ --with autoreconf

override_dh_auto_configure:
	# the latest patch is only needed on architectures without SSE or other special stuff
	if [ "${arch}" = "i386" ] || \
	   [ "${arch}" = "hurd-i386" ] ; then \
	  echo "all patches needed, do nothing here";  \
	else \
	  echo "we don't need the last patch";  \
	  quilt pop; \
	fi
	F77=gfortran dh_auto_configure -- $(CONFIGURE_FLAGS)

override_dh_auto_install:
	dh_installchangelogs NEWS.md
	dh_auto_install
	find
	$(RM) debian/tmp/usr/lib/python*/site-packages/meep/*.pyc
	$(RM) debian/tmp/usr/lib/python*/site-packages/meep/*.pyo
	$(RM) debian/tmp/usr/lib/python*/site-packages/meep/*.a
	$(RM) debian/tmp/usr/lib/python*/site-packages/meep/*.la
	$(RM) debian/tmp/usr/lib/python*/site-packages/meep/mpb/*.pyc
	$(RM) debian/tmp/usr/lib/python*/site-packages/meep/mpb/*.pyo
	$(RM) debian/tmp/usr/lib/python*/site-packages/meep/mpb/*.a
	$(RM) debian/tmp/usr/lib/python*/site-packages/meep/mpb/*.la
	$(RM) -rf debian/tmp/usr/lib/python*/site-packages/meep/__pycache__/
	$(RM) -rf debian/tmp/usr/lib/python*/site-packages/meep/adjoint/__pycache__/


override_dh_clean:
	# we do not need this anymore
	rm -f tests/latest_output
	dh_clean

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	echo ${arch}
	if [ "${arch}" = "armel" ] || \
           [ "${arch}" = "riscv64" ] || \
           [ "${arch}" = "hurd-i386" ] ; then \
	  echo "Do not make tests on this architecture" ;\
	else \
	  echo "Do make tests on this architecture" ;\
	  make check; \
	  cat tests/test-suite.log; \
	fi
#	# check whether testsuite runs everywhere
#	make check; \
#	cat tests/test-suite.log
endif

override_dh_shlibdeps:
	dh_shlibdeps
	dh_numpy3
